%option noyywrap
%option never-interactive
%option nounput yylineno
%{
	#include <stdlib.h>
	#include <math.h>
    #include <stdio.h>
    #include <string.h>
    #include <conio.h>
	
	
      char octalToDecimal(char* octal);
%}

%x STRING
%x STRING_DOUBLE_QUOTED
%x MCOMMENT
%x LCOMMENT
%x HLCOMMENT
	
DIGIT   [0-9]+
ID      [$_a-zA-Z][$_a-zA-Z0-9]*

%%

%{
    char str[1000];
	char temp[2];
%}


	//Reserved Keywords ECMAScript 2015
break		{printf("Found keyword: %s\n", yytext);}
	case		{printf("Found keyword: %s\n", yytext);}
	catch		{printf("Found keyword: %s\n", yytext);}
	class		{printf("Found keyword: %s\n", yytext);}
	const   	{printf("Found keyword: %s\n" ,yytext);}
	continue	{printf("Found keyword: %s\n" ,yytext);}
	debugger	{printf("Found keyword: %s\n" ,yytext);}
	default		{printf("Found keyword: %s\n" ,yytext);}
	delete		{printf("Found keyword: %s\n" ,yytext);}
	do			{printf("Found keyword: %s\n" ,yytext);}
	else		{printf("Found keyword: %s\n" ,yytext);}
	export		{printf("Found keyword: %s\n" ,yytext);}
	extends		{printf("Found keyword: %s\n" ,yytext);}
	finally		{printf("Found keyword: %s\n" ,yytext);}
	for			{printf("Found keyword: %s\n" ,yytext);}
	function	{printf("Found keyword: %s\n" ,yytext);}
	if			{printf("Found keyword: %s\n" ,yytext);}
	import		{printf("Found keyword: %s\n" ,yytext);}
	in			{printf("Found keyword: %s\n" ,yytext);}
	"instance of"	{printf("Found keyword: %s\n" ,yytext);}
	new			{printf("Found keyword: %s\n" ,yytext);}
	return		{printf("Found keyword: %s\n" ,yytext);}
	super		{printf("Found keyword: %s\n" ,yytext);}
	switch		{printf("Found keyword: %s\n" ,yytext);}
	this		{printf("Found keyword: %s\n" ,yytext);}
	throw		{printf("Found keyword: %s\n" ,yytext);}
	try			{printf("Found keyword: %s\n" ,yytext);}
	typeof		{printf("Found keyword: %s\n" ,yytext);}
    var     	{printf("Found keyword: %s\n", yytext);}
    void		{printf("Found keyword: %s\n" ,yytext);}
	while		{printf("Found keyword: %s\n" ,yytext);}
	with		{printf("Found keyword: %s\n" ,yytext);}
	yield		{printf("Found keyword: %s\n" ,yytext);}
	
	enum		{printf("Found keyword: %s\n" ,yytext);}
	implements	{printf("Found keyword: %s\n" ,yytext);}
	interface	{printf("Found keyword: %s\n" ,yytext);}
    let     	{printf("Found keyword: %s\n", yytext);}
	package		{printf("Found keyword: %s\n" ,yytext);}
	private		{printf("Found keyword: %s\n" ,yytext);}
	protected	{printf("Found keyword: %s\n" ,yytext);}
	public		{printf("Found keyword: %s\n" ,yytext);}
	static		{printf("Found keyword: %s\n" ,yytext);}
	await		{printf("Found keyword: %s\n" ,yytext);}
	
	abstract	{printf("Found keyword: %s\n" ,yytext);}
	boolen		{printf("Found keyword: %s\n" ,yytext);}
	byte		{printf("Found keyword: %s\n" ,yytext);}
	char		{printf("Found keyword: %s\n" ,yytext);}
	double		{printf("Found keyword: %s\n" ,yytext);}
	final		{printf("Found keyword: %s\n" ,yytext);}
	float		{printf("Found keyword: %s\n" ,yytext);}
	goto		{printf("Found keyword: %s\n" ,yytext);}
	int			{printf("Found keyword: %s\n" ,yytext);}
	long		{printf("Found keyword: %s\n" ,yytext);}
	native		{printf("Found keyword: %s\n" ,yytext);}
	short		{printf("Found keyword: %s\n" ,yytext);}
	synchronized {printf("Found keyword: %s\n" ,yytext);}
	throws		{printf("Found keyword: %s\n" ,yytext);}
	transient	{printf("Found keyword: %s\n" ,yytext);}
	volatile	{printf("Found keyword: %s\n" ,yytext);}
 
    "+"     {printf("Found Operator: +\n");}
    "-"     {printf("Found Operator: -\n");}
    "/"     {printf("Found Operator: /\n");}
    "*"     {printf("Found Operator: *\n");}
    "="     {printf("Found Operator: =\n");}
    "=="    {printf("Found Operator: ==\n");}
    "!"     {printf("Found Operator: !\n");}
    "||"    {printf("Found Operator: ||\n");}
    "&&"    {printf("Found Operator: &&\n");}
    "{"    {printf("Found Bracket: {\n");}
    "}"    {printf("Found Bracket: }\n");}
    "("    {printf("Found Bracket: (\n");}
    ")"    {printf("Found Bracket: )\n");}
	";"    {printf("Found Symbol : ;\n");}
	","    {printf("Found Symbol: ;\n");}
	
	{ID}	{printf("Found Identifier: %s\n", yytext);}
	{DIGIT} {printf("Found Integer Number: %d\n", atoi(yytext));}
	{DIGIT}+\.?{DIGIT} {printf("Found Float Number: %f\n", atof(yytext));}

\t 		;
\n		;
" "		;
\'      {strcpy(str," "); BEGIN(STRING);}
\" 		{strcpy(str," "); BEGIN(STRING_DOUBLE_QUOTED);}
        
	<STRING,STRING_DOUBLE_QUOTED>\\\'       {strcat(str, "\'");}
    <STRING,STRING_DOUBLE_QUOTED>\\\"       {strcat(str, "\"");}
    <STRING,STRING_DOUBLE_QUOTED>\\\\       {strcat(str, "\\");}
    <STRING,STRING_DOUBLE_QUOTED>\\n        {strcat(str, "\n");}
    <STRING,STRING_DOUBLE_QUOTED>\\r        {strcat(str, "\r");}
    <STRING,STRING_DOUBLE_QUOTED>\\v        {strcat(str, "\v");}
    <STRING,STRING_DOUBLE_QUOTED>\\t        {strcat(str, "\t");}
    <STRING,STRING_DOUBLE_QUOTED>\\b        {strcat(str, "\b");}
    <STRING,STRING_DOUBLE_QUOTED>\\f        {strcat(str, "\f");}
	<STRING,STRING_DOUBLE_QUOTED>\\\n		;
	<STRING,STRING_DOUBLE_QUOTED>\n		{printf("there is an error in line: %d\n",yylineno);BEGIN(INITIAL);}	
    <STRING,STRING_DOUBLE_QUOTED>\\[0-7]{1,3} {temp[0] = octalToDecimal(yytext); temp[1] = 0; strcat(str, temp );}
    <STRING,STRING_DOUBLE_QUOTED>[^\'\"\\\n\r\v\b\f]+   {strcat(str,yytext);}
	
	<STRING_DOUBLE_QUOTED><<EOF>>	{printf("The \" is not closed in line: %d\n", yylineno); BEGIN(INITIAL);}
    <STRING_DOUBLE_QUOTED>\"          {printf("string literal: %s\n",str); BEGIN(INITIAL);}
	
	<STRING><<EOF>>	{printf("The \' is not closed in line: %d\n", yylineno); BEGIN(INITIAL);}
	<STRING>\' {printf("string literal: %s\n", str); BEGIN(INITIAL);}
		
	\/\*  BEGIN(MCOMMENT);
		
	<MCOMMENT>[^*]* 
	<MCOMMENT>"*"+[^*/]* 
	<MCOMMENT>"*"+"/"  BEGIN(INITIAL);
		
		
	\/\/ {printf("One Line Comment"); BEGIN(LCOMMENT);}
	<LCOMMENT>.* { BEGIN(INITIAL);}
		
	#!	{ printf("Hashbang Comment"); BEGIN(HLCOMMENT);	}	
	<HLCOMMENT>.* { BEGIN(INITIAL);}
			
%%

int main(void){
	yylex();
}

char octalToDecimal(char* octalValue){
        char returnValue;
        int octal=0,decimal=0,i=0,j=0;
        if(strncmp(octalValue,"\\",1)==0)
            for(i=0; i<4; i++)
                octalValue[i]=octalValue[i+1];

        octalValue[3]=0;
        octal = atoi(octalValue);
        
        while(octal!=0){
            decimal =  decimal +(octal % 10)* pow(8, j++);
            octal = octal / 10;
        }
		returnValue = (char)decimal;

        return returnValue;
    }
	


   


